package request_repository;

import org.testng.annotations.DataProvider;

public class TestNG_Data_Provider {
	@DataProvider()
	public Object[][] Post_Data_Provider() {
		return new Object[][] { { "morpheus", "leader" }, { "Prasad", "SrMg" }, { "Akshay", "TL" }, };
	}

	@DataProvider()
	public Object[][] Put_Data_Provider() {
		return new Object[][] { { "morpheus", "zion resident" }, { "Prasad", "SrMg" }, { "Balaji", "TL" }, };
	}

	@DataProvider()
	public Object[][] Patch_Data_Provider() {
		return new Object[][] { { "morpheus", "zion resident" }, { "Prasad", "SrMg" }, { "Akshay", "TL" }, };
	}
	
	@DataProvider()
	public Object[][] Get_Data_Provider() {
		return new Object[][] { { "Tushar", "QA" }, { "Prasad", "SrMg" }, { "Akshay", "TL" }, };
	}

}
