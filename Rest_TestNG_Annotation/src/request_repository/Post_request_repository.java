package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import utility_common_method.Excel_Data_Extractor;
import utility_common_method.Excel_data_exctracor_2;

public class Post_request_repository {

	public static String post_request_TC1() throws IOException {
		ArrayList<String> Data = Excel_Data_Extractor.Excel_Data_Reader("Test_Data", "Post_API", "Post_TC2");
		System.out.println(Data);
		String name = Data.get(1);
		String job = Data.get(2);
		// String post_requestbody = "{\r\n" + " \"name\": \"morpheus\",\r\n" + "
		// \"job\": \"leader\"\r\n" + "}";
		String requestbody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
		
		return requestbody;

	}

}
