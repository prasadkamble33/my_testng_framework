package restassured_referance;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class Get_referance {

	public static void main(String[] args) {
		
		
		
		// Step 1- Declare the BaseURL
		RestAssured.baseURI = "https://reqres.in/";

		// Step 2 - Trigger the API
		String responsebody = given().header("Content-Type", "application/json").when().get("api/users?page=2").then()
				.log().all().extract().response().asString();
		System.out.println("responsebody is :" + responsebody);

		// Step 3 - Create an object of JsonPath to parse the response body
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_dataarray = jsp_res.getString("data");
		JSONObject res_array = new JSONObject(responsebody);
		JSONArray dataarray = res_array.getJSONArray("data");
		System.out.println(dataarray);
		
		int count = dataarray.length();
		System.out.println(count);

		for (int i = 0; i < count; i++) {
        int res_id = dataarray.getJSONObject(i).getInt("id");
        String res_first_name = dataarray.getJSONObject(i).getString("first_name");
        String res_last_name  = dataarray.getJSONObject(i).getString("last_name");
        String res_email      = dataarray.getJSONObject(i).getString("email");
        System.out.println(res_id);
        
        int expected_id[] = {7,8,9,10,11,12};
		String expected_first_name[]= {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		String expected_last_name[] = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String expected_email[] = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in",
				"tobias.funke@reqres.in","byron.fields@reqres.in","george.edwards@reqres.in",
				"rachel.howell@reqres.in"};
        
        int exp_id = expected_id[i];
        String exp_first_name = expected_first_name[i];
        String exp_last_name  = expected_last_name[i];
        String exp_email = expected_email[i];
        
        // Step 4 - Validate the reponsebody
        Assert.assertEquals(res_id,exp_id);
        Assert.assertEquals(res_first_name,exp_first_name);
        Assert.assertEquals(res_last_name,exp_last_name);
        Assert.assertEquals(res_email,exp_email);
        
		}

	}

}
