package restassured_referance;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Soap_Referance_Doller {

	public static void main(String[] args) {
		
		// Step 1 - Declare the BaseURL
		String BaseURI = "https://www.dataaccess.com";
		
		// Step 2 - Declare the Request body
		String requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "   <soapenv:Header/>\r\n"
				+ "   <soapenv:Body>\r\n"
				+ "      <web:NumberToDollars>\r\n"
				+ "         <web:dNum>100</web:dNum>\r\n"
				+ "      </web:NumberToDollars>\r\n"
				+ "   </soapenv:Body>\r\n"
				+ "</soapenv:Envelope>";
		
		// Step 3 - Trigger the API and Fetch response body
		RestAssured.baseURI= BaseURI;
		String responseBody = given().header("Content-Type","text/xml; charset=utf-8").body(requestBody).
				when().post("webservicesserver/NumberConversion.wso").then().extract().response().asString();
        
		// Step 4 - Print the response body
		System.out.println(responseBody);
		
		//Step 5 - Extract response Body parameter
		XmlPath Xml_res = new XmlPath(responseBody);
		String res_tag = Xml_res.getString("NumberToDollarsResult");
		System.out.println(res_tag);
		
		// Step 6 - Validate the response body
		Assert.assertEquals(res_tag,"one hundred dollars");
		
		

	}
}
