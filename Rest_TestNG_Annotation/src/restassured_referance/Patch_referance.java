package restassured_referance;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Patch_referance {

	public static void main(String[] args) {

		// Step 1 - Declare the BaseURL
		RestAssured.baseURI = "https://reqres.in/";

		// Step 2 - Configure the request parameter and trigger the API
		String requestbody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		String responsebody = given().header("Content-Type", "application/json")
				.body("{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}").log()
				.all().when().patch("api/users/2").then().log().all().extract().response().asString();
		//System.out.println("responsebody is :" + responsebody);

		// Step 3 - Create an object of JsonPath to parse request body and then response body
		JsonPath jsp_req = new JsonPath(requestbody);
		JsonPath jsp_res = new JsonPath(responsebody);
		
		String req_name = jsp_req.getString("name");
		String req_job  = jsp_req.getString("job");
		
		LocalDateTime currentDate = LocalDateTime.now();
		String expectedDate = currentDate.toString().substring(0,11);
		
		String res_name = jsp_res.getString("name");
		System.out.println("name is :"+res_name);
		
		String res_job = jsp_res.getString("job");
		System.out.println("job is :"+res_job);
		
		String res_updatedDate = jsp_res.getString("updatedAt");
		res_updatedDate = res_updatedDate.substring(0,11);
		System.out.println("updatedAt is :"+res_updatedDate);
		
		
		// Step 4 - Validate the reponsebody
		Assert.assertEquals(res_name,req_name);
		Assert.assertEquals(res_job,req_job);
		Assert.assertEquals(res_updatedDate,expectedDate);
		
		
	}

}
