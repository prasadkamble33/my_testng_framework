package Test_All_API_Executor;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Method.Common_Method_Handle_API;
import Endpoint.patch_endpoint;
import io.restassured.path.json.JsonPath;
import request_repository.Patch_request_repository;
import utility_common_method.Handle_API_logs;
import utility_common_method.Handle_Directory;

public class Patch_TestCase extends Common_Method_Handle_API {
	
	static File log_dir;
	static String Patch_requestBody;
	static String Patch_endpoint;
	static String Patch_responseBody;
	
	@BeforeTest
	public static void Test_Setup() throws IOException {
		log_dir = Handle_Directory.create_log_directory("Patch_TestCase_logs");
		/*String Patch_requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n"
				+ "}";
		String Patch_endpoint = "https://reqres.in/api/users/2";*/
		
		Patch_requestBody = Patch_request_repository.patch_request_TC3();
		Patch_endpoint = patch_endpoint.patch_endpoint_TC3();
		
	}

	@Test(description = "::::::::::::executing Patch API and validating the responsebody:::::::::::")
	public static void Patch_Executor() throws IOException {
		
		for (int i = 0; i < 4; i++) {
			int Patch_statuscode = patch_statuscode(Patch_requestBody, Patch_endpoint);
			System.out.println(Patch_statuscode);

			if (Patch_statuscode == 200) {
				Patch_responseBody = patch_responseBody(Patch_requestBody, Patch_endpoint);
				System.out.println(Patch_responseBody);
				Handle_API_logs.evidence_creator(log_dir,"Patch_TestCase", Patch_endpoint, Patch_requestBody, Patch_responseBody);
//				Patch_TestCase.Validator(Patch_requestBody, Patch_responseBody);
				Patch_TestCase.Validator(Patch_responseBody, Patch_responseBody);
				break;
			} else {
				System.out.println("Expected statuscode not found hence retrying");
			}
		}

	}

	public static void Validator(String Patch_requestBody, String Patch_responseBody) {
		JsonPath jsp_req = new JsonPath(Patch_requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentDate = LocalDateTime.now();
		String expectedDate = currentDate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(Patch_responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedDate = jsp_res.getString("updatedAt");
		res_updatedDate = res_updatedDate.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedDate, expectedDate);

	}

	@AfterTest
	public static void TearDown() throws IOException {
		Handle_API_logs.evidence_creator(log_dir, "Patch_TestCase", Patch_endpoint, Patch_requestBody, Patch_responseBody);

}
}
