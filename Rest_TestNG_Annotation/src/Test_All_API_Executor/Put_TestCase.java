package Test_All_API_Executor;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Method.Common_Method_Handle_API;
import Endpoint.put_endpoint;
import io.restassured.path.json.JsonPath;
import request_repository.Put_request_repository;
import utility_common_method.Handle_API_logs;
import utility_common_method.Handle_Directory;

public class Put_TestCase extends Common_Method_Handle_API {
	
	static File log_dir;
	static String Put_requestBody;
	static String Put_endpoint;
	static String Put_responseBody;
	
	@BeforeTest
	public static void Test_Setup() throws IOException {
		log_dir = Handle_Directory.create_log_directory("Put_TestCase_logs");
		//String Put_requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n"
		//		+ "}";
		//String Put_endpoint = "https://reqres.in/api/users/2";
				
		Put_requestBody = Put_request_repository.put_request_TC2();
		Put_endpoint = put_endpoint.put_endpoint_TC2();
		
	}
	
	@Test (description = "::::::::::::executing Put API and validating the responsebody:::::::::::")
	public static void Put_Executor() throws IOException {
		
		
		for (int i = 0; i < 4; i++) {
			int Put_statuscode = put_statuscode(Put_requestBody, Put_endpoint);
			System.out.println(Put_statuscode);

			if (Put_statuscode == 200) {
				Put_responseBody = put_responseBody(Put_requestBody, Put_endpoint);
				System.out.println(Put_responseBody);
				Handle_API_logs.evidence_creator(log_dir,"Put_TestCase", Put_endpoint, Put_requestBody, Put_responseBody);
//				Put_TestCase.Validator(Put_requestBody, Put_responseBody);
				Put_TestCase.Validator(Put_requestBody, Put_responseBody);
				break;
			} else {
				System.out.println("Expected statuscode not found hence retrying");
			}
			
		}
	}
		public static void Validator (String requestbody, String responsebody) {
			JsonPath jsp_req = new JsonPath(requestbody);
			String req_name = jsp_req.getString("name");
			String req_job  = jsp_req.getString("job");
		    LocalDateTime currentDate = LocalDateTime.now();
			String expectedDate = currentDate.toString().substring(0,11);
			
			JsonPath jsp_res = new JsonPath(responsebody);
			String res_name = jsp_res.getString("name");			
			String res_job = jsp_res.getString("job");
			String res_updatedDate = jsp_res.getString("updatedAt");
			res_updatedDate = res_updatedDate.substring(0,11);
			
			Assert.assertEquals(res_name,req_name);
			Assert.assertEquals(res_job,req_job);
			Assert.assertEquals(res_updatedDate,expectedDate);
			
		}
		@AfterTest
		public static void TearDown() throws IOException {
			Handle_API_logs.evidence_creator(log_dir, "Put_TestCase", Put_endpoint, Put_requestBody, Put_responseBody);

		}
	}


