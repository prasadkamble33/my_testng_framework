package Test_All_API_Executor;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Method.Common_Method_Handle_API;
import Endpoint.post_endpoint;
import io.restassured.path.json.JsonPath;
import request_repository.Post_request_repository;
import utility_common_method.Handle_API_logs;
import utility_common_method.Handle_Directory;

public class Post_TestCase extends Common_Method_Handle_API {
	static File Log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@BeforeTest
	public static void Test_Setup() throws IOException {
		Log_dir = Handle_Directory.create_log_directory("Post_TestCase_logs");
		// String Post_requestBody = "{\r\n" + " \"name\": \"morpheus\",\r\n" + "
		// \"job\": \"leader\"\r\n" + "}";
		// String Post_endpoint = "https://reqres.in/api/users";

		requestBody = Post_request_repository.post_request_TC1();
		endpoint = post_endpoint.post_endpoint_TC1();

	}

	@Test(description = "::::::::::::executing post API and validating the responsebody:::::::::::")
	public static void Post_Executor() throws IOException {

		for (int i = 0; i < 4; i++) {
			int Post_statuscode = Post_Statuscode(requestBody, endpoint);
			System.out.println(Post_statuscode);

			if (Post_statuscode == 201) {
				responseBody = Post_ResponseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Handle_API_logs.evidence_creator(Log_dir, "Post_TestCase", endpoint, requestBody,
						responseBody);
				Post_TestCase.validator(requestBody, responseBody);
				break;
			} else {
				System.out.println("Expected statuscode not found Hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expectedate = currentdate.toString().substring(0, 10);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		String res_createAtdate = jsp_res.getString("createdAt");
		res_createAtdate = res_createAtdate.substring(0, 10);

		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createAtdate, expectedate);

	}

	@AfterTest
	public static void TearDown() throws IOException {
		Handle_API_logs.evidence_creator(Log_dir, "Post_TestCase", endpoint, requestBody, responseBody);

	}

}
