package Test_All_API_Executor;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Method.Common_Method_Handle_API;
import Endpoint.get_endpoint;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import utility_common_method.Handle_API_logs;
import utility_common_method.Handle_Directory;

public class Get_TestCase extends Common_Method_Handle_API {
	
	static File log_dir;
	static String Get_endpoint;
	static String Get_responseBody;
	
	@BeforeTest
	public static void Test_Setup() throws IOException {
		log_dir = Handle_Directory.create_log_directory("Get_TestCase_logs");
		//String Get_endpoint = "https://reqres.in/api/users?page=2";
		Get_endpoint = get_endpoint.get_endpoint();	
	}
	
	@Test(description = "::::::::::::executing Get API and validating the responsebody:::::::::::")
	public static void Get_Executor() throws IOException {
		
		for (int i = 0; i < 4; i++) {
			int Get_statuscode = get_statuscode(Get_endpoint);
			System.out.println(Get_statuscode);

			if (Get_statuscode == 200) {
				Get_responseBody = get_responseBody(Get_endpoint);
				System.out.println(Get_responseBody);
				Handle_API_logs.evidence_creator(log_dir,"Get_TestCase",Get_endpoint,null, Get_responseBody);
				Get_TestCase.Validator(Get_responseBody);
				break;
			} else {
				System.out.println("Expected statuscode not found hence retrying");
			}

		}

	}

	public static void Validator(String Get_responseBody) {
		int expected_id[] = {7,8,9,10,11,12};
		String expected_first_name[]= {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		String expected_last_name[] = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String expected_email[] = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in",
				"tobias.funke@reqres.in","byron.fields@reqres.in","george.edwards@reqres.in",
				"rachel.howell@reqres.in"};
		
		JsonPath jsp_res = new JsonPath(Get_responseBody);
		String res_dataarray = jsp_res.getString("data");
		JSONObject res_array = new JSONObject(Get_responseBody);
		JSONArray dataarray = res_array.getJSONArray("data");
		
		int count = dataarray.length();
		for (int i = 0; i < count; i++) {
        int res_id = dataarray.getJSONObject(i).getInt("id");
        String res_first_name = dataarray.getJSONObject(i).getString("first_name");
        String res_last_name  = dataarray.getJSONObject(i).getString("last_name");
        String res_email      = dataarray.getJSONObject(i).getString("email");
        
        int exp_id = expected_id[i];
        String exp_first_name = expected_first_name[i];
        String exp_last_name  = expected_last_name[i];
        String exp_email = expected_email[i];
        
        Assert.assertEquals(res_id,exp_id);
        Assert.assertEquals(res_first_name,exp_first_name);
        Assert.assertEquals(res_last_name,exp_last_name);
        Assert.assertEquals(res_email,exp_email);
		}
		
	}

		@AfterTest
		public static void TearDown() throws IOException {
			Handle_API_logs.evidence_creator(log_dir, "Get_TestCase", Get_endpoint,null, Get_responseBody);
		
}
}
