package API_Common_Method;

import static io.restassured.RestAssured.given;

public class Common_Method_Handle_API {

	public static int Post_Statuscode(String requestBody, String endpoint) {

		int post_statuscode = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
				.then().extract().statusCode();
		return post_statuscode;
	}

	public static String Post_ResponseBody(String requestBody, String endpoint) {
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
				.then().extract().response().asString();
		return responseBody;
	}

///////////////////////////////////////////////////////////////////////////////////////////////////
	public static int put_statuscode(String requestBody, String endpoint) {

		int put_statuscode = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().statusCode();
		return put_statuscode;
	}

	public static String put_responseBody(String requestBody, String endpoint) {
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().response().asString();
		return responseBody;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int patch_statuscode(String requestBody, String endpoint) {
		int patch_statuscode = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().statusCode();
		return patch_statuscode;
	}

	public static String patch_responseBody(String requestBody, String endpoint) {
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().response().asString();
		return responseBody;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int get_statuscode(String endpoint) {
		int get_statuscode = given().header("Content-Type", "application/json").when().get(endpoint).then().extract()
				.statusCode();
		return get_statuscode;
	}

	public static String get_responseBody(String endpoint) {
		String responseBody = given().header("Content-Type", "application/json").when().get(endpoint).then().extract()
				.response().asString();
		return responseBody;
	}
/////////////////////////////////////////////////////////////////////////////////////	

	public static int delete_statuscode(String endpoint) {
		int delete_statuscode = given().header("Content-Type", "application/json").when().delete(endpoint).then()
				.extract().statusCode();
		return delete_statuscode;
	}

}
