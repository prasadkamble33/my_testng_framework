package Driver_Package;

import java.io.IOException;

import API_Common_Method.Common_Method_Handle_API;
import Test_All_API_Executor.Post_TestCase;

public class Driver_Class extends Common_Method_Handle_API{

	public static void main(String[] args) throws IOException {
		
		String post_requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		String post_endpoint = "https://reqres.in/api/users";
		int statuscode = Post_Statuscode(post_requestBody, post_endpoint);
		System.out.println(statuscode);
		
		String post_responseBody = Post_ResponseBody(post_requestBody, post_endpoint);
		System.out.println(post_responseBody);

		String put_requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String put_endpoint = "https://reqres.in/api/users/2";
		int put_statuscode = put_statuscode(put_requestBody, put_endpoint);
	    System.out.println(put_statuscode);
	    
	    String put_responseBody = put_responseBody(put_requestBody, put_endpoint);
	    System.out.println(put_responseBody);
		

		String patch_requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String patch_endpoint = "https://reqres.in/api/users/2";
		int patch_statuscode = patch_statuscode(patch_requestBody,patch_endpoint);
		System.out.println(patch_statuscode);
		
		String patch_responseBody = patch_responseBody(patch_requestBody, patch_endpoint);
		System.out.println(patch_responseBody);
		
		
		String get_endpoint = "https://reqres.in/api/users?page=1";
		int get_statuscode = get_statuscode(get_endpoint);
		System.out.println(get_statuscode);
		
		String get_responseBody = get_responseBody(get_endpoint);
		System.out.println(get_responseBody);
		
		
		
	}

	
	
	
	
	
	
}
